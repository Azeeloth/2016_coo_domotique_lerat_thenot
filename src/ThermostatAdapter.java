import thermos.Thermostat;

/**
 * Created by RAPHAEL on 11/05/2016.
 */
public class ThermostatAdapter extends Thermostat implements Appareil{

    public ThermostatAdapter(){
        super();
    }
    @Override
    public void allumer() {
        super.monterTemperature();
    }

    @Override
    public void eteindre() {
        super.baisserTemperature();
    }

}