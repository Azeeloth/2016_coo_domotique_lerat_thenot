import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TelecommandeTest {
	
	@Test
	/**
	 * test ajout lampe telec vide
	 */
	public void testAjoutLampe_1() {
		Lampe l = new Lampe("lampe1");
		Telecommande tl = new Telecommande();
		// methode testee
		tl.ajouterAppareil(l);

		// verification
		assertEquals("une lampe devrai etre ajouter", 1, tl.listobj.size());
	}
	
	@Test
	/**
	 * test ajout lampe telec 1 element
	 */
	public void testAjoutLampe_2() {
		Lampe l = new Lampe("lampe1");
		Lampe l2 = new Lampe("lampe2");
		Telecommande tl = new Telecommande();
		// methode testee
		tl.ajouterAppareil(l);
		tl.ajouterAppareil(l2);

		// verification
		assertEquals("une lampe devrai etre ajouter", 2, tl.listobj.size());
	}
	
	@Test
	/**
	 * test activer lampe position 0
	 */
	public void testActiverLampe_1() {
		Lampe l = new Lampe("lampe1");
		Telecommande tl = new Telecommande();
		tl.ajouterAppareil(l);
		// methode testee
		tl.activerAppareil(0);
		
		// verification
		assertEquals("lampe1 devrai etre activer", true, ((Lampe) tl.listobj.get(0)).isAllume());
	}
	
	@Test
	/**
	 * test activer lampe position 1
	 */
	public void testActiverLampe_2() {
		Lampe l = new Lampe("lampe1");
		Lampe l2 = new Lampe("lampe2");
		Telecommande tl = new Telecommande();
		tl.ajouterAppareil(l);
		tl.ajouterAppareil(l2);
		// methode testee
		tl.activerAppareil(1);
		
		// verification
		assertEquals("lampe2 devrai etre activer", true, ((Lampe) tl.listobj.get(1)).isAllume());
	}
	
	
	@Test
	/**
	 * test activation lampe inexistante
	 */
	public void testActiverLampe_3() {
		Telecommande tl = new Telecommande();
		tl.ajouterAppareil(null);
		// methode testee
		tl.activerAppareil(0);
		
		// verification
		assertEquals("lampe1 n'existe pas", null, tl.listobj.get(0));
	}
	
	
	
	
	@Test
	/**
	 * test activation lumiere
	 */
	public void testActiverLumiere() {
		Telecommande tl = new Telecommande();
		tl.ajouterAppareil(new LumiereAdapter());
		// methode testee
		tl.activerAppareil(0);
		
		// verification
		assertEquals("lampe1 n'existe pas", 10, ((LumiereAdapter) tl.listobj.get(0)).getLumiere());
	}
	
	
	@Test
	/**
	 * test descactivation lumiere
	 */
	public void testDesactiverLumiere() {
		Telecommande tl = new Telecommande();
		tl.ajouterAppareil(new LumiereAdapter());
		// methode testee
		tl.desactiverAppareil(0);
		
		// verification
		assertEquals("lampe1 n'existe pas", 0, ((LumiereAdapter) tl.listobj.get(0)).getLumiere());
	}
	
}
