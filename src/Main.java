
public class Main {

	public static void main(String args[])
	{
		Telecommande t=new Telecommande();
		
		
		Lampe l1=new Lampe("Lampe 1");
		t.ajouterAppareil(l1);
		t.ajouterAppareil(new Hifi());
		t.ajouterAppareil(new LumiereAdapter());
		t.ajouterAppareil(new ThermostatAdapter());


		TelecommandeGraphique tg=new TelecommandeGraphique(t);
		
	}
	
}
