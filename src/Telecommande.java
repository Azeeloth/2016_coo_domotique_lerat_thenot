import java.util.ArrayList;
import java.util.List;

public class Telecommande {

	List<Appareil> listobj;

	public Telecommande() {
		listobj = new ArrayList<Appareil>();
	}

	public void ajouterAppareil(Appareil a) {
		listobj.add(a);
	}

	public void activerAppareil(int indiceAppareil) {
		if (indiceAppareil <= listobj.size() && listobj.get(indiceAppareil) != null) {
			listobj.get(indiceAppareil).allumer();
		}
	}

	public void desactiverAppareil(int indiceAppareil) {
		if (indiceAppareil <= listobj.size() && listobj.get(indiceAppareil) != null) {
			listobj.get(indiceAppareil).eteindre();
		}
	}

	public void activerTout() {
		for (Appareil a : listobj) {
			a.allumer();
		}
	}

	public int getNombre() {
		return listobj.size();
	}

	public String toString() {
		StringBuffer str = new StringBuffer("Est lie a : " + listobj.size() + " appareil");
		for (Appareil a : listobj) {
			str.append("\n");
			str.append(a.toString());
		}
		return str.toString();
	}

}
