
public class LumiereAdapter extends Lumiere implements Appareil{

	
	public LumiereAdapter(){
		super();
	}

	@Override
	public void allumer() {
		super.changerIntensite(10);
		
	}

	@Override
	public void eteindre() {
		super.changerIntensite(0);
		
	}
	
	
}
